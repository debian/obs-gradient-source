obs-gradient-source (0.3.2-1) unstable; urgency=medium

  * New upstream version 0.3.2.
  * debian/control: added a Breaks field to make this plugin incompatible with
    obs-text-slideshow, no longer in Debian.
  * debian/copyright: updated upstream copyright years.
  * debian/patches/: removed all patches (010_fix-format-truncation.patch and
    020_add-pt-BR-translation.patch) because the current source code has been
    fixed. Thanks!

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Tue, 27 Jun 2023 20:55:30 -0300

obs-gradient-source (0.3.1-4) unstable; urgency=medium

  * debian/rules: removed temporary workaround to exclude extra installed
    files in favor of the '-DLINUX_PORTABLE=Off' argument for CMake.
  * debian/upstream/metadata: updated Donation field.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Tue, 21 Feb 2023 18:32:41 -0300

obs-gradient-source (0.3.1-3) unstable; urgency=medium

  * debian/control:
      - Added a relationship for libobs-dev.
      - Added Enhances field.
      - Added obs-studio to Depends field.
      - Set 'Multi-Arch: same'.
  * debian/rules: fixed execute_after_dh_install target.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Wed, 08 Feb 2023 01:46:50 -0300

obs-gradient-source (0.3.1-2) unstable; urgency=medium

  * Upload to unstable.
  * debian/rules: added a target to check if translation to pt_BR is updated.
  * debian/watch: using filenamemangle to generate right names for downloaded
    files.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Thu, 02 Feb 2023 23:33:23 -0300

obs-gradient-source (0.3.1-1) experimental; urgency=medium

  * Initial release. (Closes: #1029973)

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Sun, 29 Jan 2023 15:03:47 -0300
